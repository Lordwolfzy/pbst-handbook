module.exports = [{
    text: 'Home',
    link: '/de/'
},
{
    text: 'PBST Handbuch',
    link: '/de/pbst/'
},
{
    text: 'Danksagung',
    link: '/de/credits/'
},
{
    text: 'Pinewood',
    items: [{
            text: 'PET-Handbuch',
            link: 'https://pet.pinewood-builders.com'
        },
        {
            text: 'TMS-Handbuch',
            link: 'https://tms.pinewood-builders.com'
        }
    ]
}
]