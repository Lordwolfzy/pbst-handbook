module.exports = ctx => ({
    head: [
        ['link', {
            rel: 'icon',
            href: '/PBST-Logo.png'
        }],
        ['link', {
            rel: 'manifest',
            href: '/manifest.json'
        }],
        ['meta', {
            name: 'theme-color',
            content: '#3eaf7c'
        }],
        [`script
  type="text/javascript"
  src="https://crowdin.com/js/crowdjet/crowdjet.js"`, {}, ``],
    [`div id="crowdjet-container"
  data-organization-domain="pinewood-builders"
  data-project-id="4"
  style="bottom: 90px; right: 20px;"`, {}, ``]
    ,[`div
  id="crowdjet-expand-container"
  style="bottom: 10px; right: 20px;"`, {}, ``]
    ],
    dest: 'public/',
    locales: {
        '/': {
            lang: 'en-US',
            title: 'PBST Handbook',
            description: 'The official PBST Handbook'
        },
        '/fi/': {
            lang: 'fi-FI',
            title: 'PBST-käsikirja',
            description: 'Virallinen PBST-käsikirja'
        },
        '/es/': {
            lang: 'es-ES',
            title: 'Manual del PBST',
            description: 'El manual oficial del PBST'
        }
    },
    themeConfig: {
        repo: 'https://gitlab.com/TCOOfficiall/PBST-Handbook',
        editLinks: true,
        docsDir: 'docs/',
        logo: '/PBST-Logo.png',
        smoothScroll: true,
        sidebarDepth: 3,
        yuu: {
            defaultDarkTheme: true,
        },

        algolia: ctx.isProd ? ({
            apiKey: '335b94bd05315a8ed572b77d95e6d5b7',
            indexName: 'pbst'
        }) : null,
        locales: {
            '/': {
                label: 'English (English)',
                selectText: 'Languages',
                ariaLabel: 'Select language',
                editLinkText: 'Edit this page on GitLab',
                lastUpdated: 'Last Updated',
                nav: require('./nav/en'),
                sidebar: {
                    '/pbst/': [
                        'handbook/',
                        'ranks-and-ranking-up/',
                        'training-hosters/'
                    ]
                }
            },
            '/fi/': {
                label: 'Finnish (Suomi)',
                selectText: 'Kielet',
                ariaLabel: 'Valitse kieli',
                editLinkText: 'Muokkaa tätä sivua GitLabissa',
                lastUpdated: 'Viimeksi päivitetty',
                nav: require('./nav/fi'),
                sidebar: {
                    '/fi/pbst/': [
                        'handbook/',
                        'ranks-and-ranking-up/',
                        'training-hosters/'
                    ]
                }
            },
            '/es/': {
                label: 'Español (España)',
                selectText: 'Idiomas',
                ariaLabel: 'Selecciona un idioma',
                editLinkText: 'Edita esta página en GitLab',
                lastUpdated: 'Última actualización',
                nav: require('./nav/es'),
                sidebar: {
                    '/es/pbst/': [
                        'handbook/',
                        'ranks-and-ranking-up/',
                        'training-hosters/'
                    ]
                }
            },            
        }

    },
    
    plugins: [
        [
            'vuepress-plugin-zooming',
            {
                selector: '.theme-default-content img.zooming',
                delay: 1000,
                options: {
                    bgColor: 'white',
                    zIndex: 10000,
                },
            },
        ],
        ['@vuepress/pwa',
            {
                serviceWorker: true,
                updatePopup: true
            }
        ],
        [
          '@vuepress/google-analytics',
          {
            'ga': 'UA-168777162-2' // UA-00000000-0
          }
        ]
    ],
})


