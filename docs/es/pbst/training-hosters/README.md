# Comandos de Tier 4 y Entrenador

:::warning Estos comandos son para entrenamientos "la mayoría" del tiempo. 
Varios de los comandos vistos en esta página, serán utilizados por asistentes, Tiers 4 y Entrenadores. Cada instalación de PB tiene un sistema de permisos, así que usar estos comandos como un rango inferior a Tier 4 no será posible.

Cuando se organiza un entrenamiento o se participa en uno como asistente. El entrenamiento se imparte en **INGLÉS**. Así que asegúrate de hablarlo con fluidez, o al menos comprensible. 
:::

:::danger No solo copies y pegues 
Por favor ten en cuenta que estos comandos son **EJEMPLOS**, tienes que cambiarlos a tu entrenamiento/lo que necesites que haga.   
Asegúrate de que has probado tus comandos antes de usarlos en un entrenamiento **REAL**... 
:::

## Posibles argumentos de comandos
- `me` -> Tú mismo.
- `player-name` -> Un jugador que seleccionas.
- `all` -> Todos los jugadores en el servidor (incluidos tú y los observadores).
- `%team-name` -> Seleccionar un equipo (**Ejemplo**: `;add %Winners Win 1`).
- `nonadmins` -> Todos los usuarios que no tienen permisos de administrador (probablemente todos con rango inferior a Tier 4. **NOTA**: Esto significa que los observadores también pueden ser seleccionados con este argumento).
- `radius-<distance>` -> Todos los jugadores alrededor de ti dentro de la distancia que especificas. (**Ejemplo**: `;team radius-20 winners`)
- `others` -> Esto selecciona a todos menos a ti (si tienes asistentes u observadores, esto también les seleccionará)

## ¿Qué comandos se utilizan generalmente en los entrenamientos?
- `;fly | ;god`  
  Esta cadena de comandos te da vuelo, y te hace dios (no puedes recibir ningún daño)
- `;team <argument> <team>`  
  Pone a un jugador en un equipo, dependiendo de qué equipos creaste con los siguientes comandos especificados abajo
- `;newteam Host/Assistants <color>`  
  Cuando estás organizando un entrenamiento, este comando crea el equipo Anfitrión/Asistente.
- `;newteam Observers <color>`  
  Crea un equipo para los observadores (personas que observan el entrenamiento. **NOTA**: Estos nunca obtienen puntos)
- `;newteam Winners <color>`  
  Un equipo para definir a los ganadores en una partida y darles victorias (así no le das a todos una victoria)
- `;newteam Security <color>`  
  Este es el equipo por defecto, donde todos deben estar (excepto el anfitrión, los observadores y los asistentes) ![img](https://i.imgur.com/vLwSyFK.png)

- `;addstat Wins`  
  Añade una estadística para que puedas ver cuántas victorias tienen todos, y para registrarlas
- `;m <message>`  
  Este es un anuncio que puedes ver en pantalla con la línea de roblox predeterminada. ![img](https://i.imgur.com/PcjEr9v.png)
- `;n <message>`  
  Una pequeña nota que todo el mundo ve en su pantalla ![img](https://i.imgur.com/rZKvc03.png)
- `;h <message>` ![img](https://i.imgur.com/YXRuuJB.png) Un mensaje enviado a la parte superior de la pantalla de todos
- `;add <argument> Wins 1`  
  Le da a un jugador o a un equipo una victoria (los jugadores no necesitan un %, pero los equipos sí).
- `;team radius-<distance> <Team>`  
  Pone a la gente a tu alrededor en un equipo, esto es muy útil para otorgar victorias
- `;give <argument> <tool>`  
  Da a la gente una herramienta que se encuentra en la caja de herramientas
- `;sword <argument>`  
  Un comando corto para dar espadas a la gente
- `;removetools <argument>`  
  Elimina las herramientas de las personas seleccionadas
- `;viewtools <argument>`  
  Te permite ver las herramientas que una persona tiene en su inventario
- `!importalias <!alias> <Command>`  
  Utiliza este comando para importar un alias a tu propia colección (Ejemplo: **!importalias !shirtme !shirt [146487695](https://www.roblox.com/catalog/146487695/Security-Vest-Specops-PB)**)

## Sugerencias para alias

 1. Abre Kronos haciendo clic en la esquina inferior derecha de tu pantalla (logo del PBST)
 2. Abre la pestaña "Alias" (aliases)
 3. Presiona "Añadir" (Add) en la parte inferior de la ventana.
 4. Rellena el alias que deseas para tu comando, en el campo **Alias:**
 5. Introduce uno de los comandos aquí, o uno de tus propios comandos

**`;assist`**:
- `;team <arg1> Host/Assistants | ;fly <arg1> | ;god <arg1> | ;sword <arg1>` Otorga comandos a una persona asistente y la pone en el equipo de Anfitrión/Asistentes. Los Entrenadores también pueden usar comandos extra para mejorar el alias:
- `;team <arg1> Host/Assistants | ;admin <arg1> | ;loadout <arg1> Special` Otorga a la persona permisos de moderador temporal para el entrenamiento y el armamento de Nivel 4 (Defensa especial).

**`;observe`**:
- `;team <arg1> Obs | ;fly <arg1> | ;god <arg1>`   
  Coloca a alguien en el equipo observador y le da comandos de observador  
  ![img](https://i.imgur.com/imuV43Y.png)

**`;s2vote`**:
- `;vote nonadmins,-%ob Sword_Fight,Guns,Juggernaut,Bombs 20 ¿Que actividad vamos a hacer?` Útil para que los anfitriones puedan seleccionar fácilmente una arena de combate del Sector 2.

**`;noreturn`**:
- `;lobbydoors off | ;teleporters off`  
  Bloquea el lobby para que nadie pueda salir de él

**`;bafk`**:
- `;bring <arg1> | ;afk <arg1>` Trae la persona hacia ti y la pone en AFK.

**`;flygod`**:
- `;fly <arg1> | ;god <arg1>` Esto permite al argumento seleccionado volar and ser inmunizado.

**`;bjail`**:
- `;bring <arg1> | ;jail <arg1>` Trae a la persona y la encarcela delante de ti.

**`;win20`**:
- `;team radius-20 %Winners`

**`;twin`**:
- `;add %Winners Wins 1`

**`;unteamwin`**:
- `;unteam %Winners` Este proceso se utiliza para dar victorias a un grupo determinado de personas fácilmente.

**`;trainingsetup`**:
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on | ;addstat Strikes` (Este también se puede utilizar con **`;strike`**) Crea todos los equipos que necesites además de dar la estadística de 'Victorias'. Para hacer el alias más avanzado, hay varios publicados en esta línea.


**Créditos a *MarusiFyren* y *EquilibriumCurse* por ayudarme a crear esta página, y Marusi por algunas de las capturas de pantalla utilizadas en esta página.**